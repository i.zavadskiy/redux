/**
 * Created by zavadskyi on 12/5/17.
 */
import createStore from '../myRedux/store'

describe('createStore function', () => {
  let initState = {
    reducer1: {
      data: 'text'
    },
    reducer2: {
      data: 'text'
    },
  };

  const reducer = jest.fn();
  const callback = jest.fn();

  const action = {
    type: 'test',
    data: 'Hello world'
  };
  const invalidAction = {
    data: 'Hello world'
  };

  reducer.mockReturnValueOnce({
    reducer1: {
      data: 'new'
    },
    reducer2: {
      data: 'new'
    },
  });

  const store = createStore(initState, reducer)

  test('createStore return three functions in object', () => {

    expect(typeof store).toBe('object');
    expect(typeof store.dispatch).toBe('function');
    expect(typeof store.getState).toBe('function');
    expect(typeof store.subscribe).toBe('function');

  });


  describe('store functions happy pass :)', () => {

    test('getState return valid state', () => {
      expect(store.getState()).toEqual({
        reducer1: {
          data: 'text'
        },
        reducer2: {
          data: 'text'
        },
      });
    });

    test('subscribe return unsubscribe', () => {
      expect(typeof store.subscribe(callback)).toBe('function');

    });

    test('dispatch update state and call functions from subscribe', () => {
      expect(() => store.dispatch(action)).not.toThrow();
      expect(reducer.mock.calls.length).toBe(1);
      expect(store.getState()).toEqual({
        reducer1: {
          data: 'new'
        },
        reducer2: {
          data: 'new'
        },
      });

    });

  });

  describe('store functions sad pass :(', () => {

    test('subscribe throw error when call wo arguments', () => {
      expect(()=>store.subscribe()).toThrow();
    });

    test('dispatch throw error with invalid action', () => {
      expect(() => store.dispatch(invalidAction)).toThrow();
    });

  });


})
;