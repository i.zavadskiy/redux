/**
 * Created by zavadskyi on 12/1/17.
 */
import combineReducers from '../myRedux/combineReducers'


describe('combineReducer function', () => {
  const reducer1 = jest.fn();
  const reducer2 = jest.fn();

  reducer1.mockReturnValueOnce({
    data: 'Hello world'
  });
  reducer2.mockReturnValueOnce({
    data: 'text for 2'
  });

  const action = {
    type: 'test',
    data: 'Hello world'
  };
  const state = {
    reducer1: {
      data: 'text for 1'
    },
    reducer2: {
      data: 'text for 2'
    },
  };

  test('return function combine', () => {

    expect(typeof combineReducers({reducer1, reducer2})).toBe("function");

  });

  test('combineReducers throw error without arguments', () => {
    expect(combineReducers()).toThrow();
  });

  describe('combine function', () => {
    const combine = combineReducers({reducer1, reducer2});

    test('combine function correct work with reducers', () => {

      expect(combine(state, action)).toEqual(
        {
          reducer1: {
            data: 'Hello world'
          },
          reducer2: {
            data: 'text for 2'
          },
        }
      );

      expect(reducer1.mock.calls.length).toBe(1);
      expect(reducer1.mock.calls[0][1]).toEqual({
        data: 'text for 1'
      });
      expect(reducer1.mock.calls[0][1]).not.toEqual({
        data: 'text for 2'
      });
      expect(reducer2.mock.calls.length).toBe(1);
      expect(reducer2.mock.calls[0][1]).toEqual({
        data: 'text for 2'
      });
      expect(reducer2.mock.calls[0][1]).not.toEqual({
        data: 'text for 1'
      });
    });

    describe('combine wrong way', () => {
      test('combine throw error with wrong arguments', () => {

        expect(combine(action)).not.toEqual(
          {
            reducer1: {
              data: 'Hello world'
            },
            reducer2: {
              data: 'text for 2'
            },
          }
        );

        expect(combine(state)).not.toEqual(
          {
            reducer1: {
              data: 'Hello world'
            },
            reducer2: {
              data: 'text for 2'
            },
          }
        );
      });
    });

  });

});
