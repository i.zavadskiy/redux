/**
 * Created by zavadskyi on 12/20/17.
 */
import Test from '../components/component';
import {shallow, mount} from 'enzyme';
import React from 'react';
import PropTypes from 'prop-types';


describe('Component', () => {
  it('should render a props data and trigger click', () => {
    const click = jest.fn();

    const wrapper = mount(
      <Test data="lol" onClickButton={click} />
    );
    console.log(wrapper.text());
    expect(wrapper.text()).toBe('test lollol');
    wrapper.find('[name="click"]').simulate('click');
    expect(click).toHaveBeenCalledTimes(1);

  });

});