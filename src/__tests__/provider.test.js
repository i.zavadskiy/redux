/**
 * Created by zavadskyi on 12/6/17.
 */
import {shallow, mount} from 'enzyme';
import Provider from '../react-redux/provider/provider'
import React from 'react';


describe('Provider component', () => {

  it('should render Provider with child component', () => {
    const wrapper = shallow(
      <Provider>
        <div></div>
      </Provider>
    );
     expect(wrapper).toMatchSnapshot();
  });

  it('should take store object', () => {
    const dispatch = jest.fn();
    const store = {
      dispatch
    };
    const wrapper = mount(
      <Provider store={store}>
        <div></div>
      </Provider>
    );
    expect( typeof wrapper.prop('store')).toBe('object');
  });

});