/**
 * Created by zavadskyi on 12/6/17.
 */
import connect from '../react-redux/connect/сonnect';
import {shallow} from 'enzyme';
import React from 'react';
import PropTypes from 'prop-types';

describe('Connect function', () => {
  const mapStateToProps = jest.fn();
  const mapDispatchToProps = jest.fn();
  const subscribe = jest.fn();
  const getState = jest.fn();
  const dispatch = jest.fn();

  const store = {
    subscribe,
    getState,
    dispatch
  };

  const componentFunc = connect(mapStateToProps, mapDispatchToProps);
  test('connect return function', () => {

    expect(typeof componentFunc).toBe('function')
  });

  describe('component function', () => {
    const Comp = componentFunc();
    // const context = {store: {store}};
    test('return componеnt', () => {

      const wrapper = shallow(<Comp />, {
        context: {store: store},
        childContextTypes: {context: PropTypes.object}
      });

      expect(getState.mock.calls.length).toBe(1);
      expect(subscribe.mock.calls.length).toBe(1);
    });
  });
});