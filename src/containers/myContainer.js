/**
 * Created by zavadskyi on 11/29/17.
 */
import connect from '../react-redux/connect/сonnect'
import Test from '../components/component'
import {myAction} from '../actions/actions';


let mapStateToProps = (store, props) => ({
  data: store.reducerForC1.data,
  parentProp: props.data
});

let mapDispatchToProps = (dispatch) => ({
  onClickButton: () => {dispatch(myAction())}
});


let Conn = connect(mapStateToProps,mapDispatchToProps)(Test);

export default Conn;