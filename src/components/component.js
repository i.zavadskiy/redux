/**
 * Created by zavadskyi on 11/29/17.
 */
import React, {Component} from 'react';


class Test extends React.Component {
  constructor() {
    super();
  }

  render(){
    return(
      <div>
        test {this.props.data}
        <button name="click" onClick={this.props.onClickButton}>lol</button>
      </div>

    )
  }

}

export default Test;