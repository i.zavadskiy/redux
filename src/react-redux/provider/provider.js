/**
 * Created by zavadskyi on 11/28/17.
 */
import {Component, Children} from 'react'
import React from 'react';
import PropTypes from 'prop-types';


class Provider extends Component {

  getChildContext() {
    return {store: this.props.store};
  }

  constructor(props, context) {
    super(props, context);
    // this['store'] = props.store;
  }

  render() {
    return Children.only(this.props.children);
  }

}
Provider.childContextTypes = {
  store: PropTypes.object
};


export default Provider;