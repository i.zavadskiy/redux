/**
 * Created by zavadskyi on 11/28/17.
 */
import Provider, { createProvider }  from './provider/provider';

export {Provider, createProvider};