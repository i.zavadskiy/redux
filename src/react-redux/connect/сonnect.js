/**
 * Created by zavadskyi on 11/29/17.
 */
import React, {Component, createElement} from 'react';
import PropTypes from 'prop-types';

export default function connect(mapStateToProps=null, mapDispatchToProps=null) {
  return function myComp(component=null) {
    class Comp extends React.Component {
      constructor() {
        super();
        this.forSubscribe = this.forSubscribe.bind(this);
      }

      forSubscribe(){
        this.forceUpdate();
      }

      render(){
        if(unsubscribe != undefined){
          unsubscribe();
        }
        let store = this.context.store;
        var unsubscribe = store.subscribe(this.forSubscribe);
        let state = store.getState();
        let dispatch = store.dispatch;

        return(createElement(component, createProps(mapStateToProps, mapDispatchToProps, state, dispatch, this.props) ))

      }

    }

    Comp.contextTypes = {
      store: PropTypes.object
    };

    return Comp;
  }
}


function createProps(mapStateToProps=null, mapDispatchToProps=null, state, dispatch, props=null){
 return Object.assign({},mapStateToProps(state, props),mapDispatchToProps(dispatch, props));
}