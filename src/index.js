import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from './react-redux/index'
import App from './App';
import  {
  createStore,
  combineReducers,
} from './myRedux/index';
import reducer1 from './reducers/reducer1';
import reducer2 from './reducers/reducer2';
import  {nativeApplyMiddlewares} from './myRedux/index';
import ping from './middlewares/ping';
import log from './middlewares/log';

let initState = {
  reducerForC1: {
    data: 'text'
  },
  reducerForC2: {
    data: 'text'
  },
};

let reducerForC1 = reducer1;
let reducerForC2 = reducer2;

let reducer = combineReducers({reducerForC1, reducerForC2});
let store = createStore(initState, reducer);
store = nativeApplyMiddlewares(store, [ping, log]);
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root'));

