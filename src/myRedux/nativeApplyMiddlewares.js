/**
 * Created by zavadskyi on 11/27/17.
 */
export default function nativeApplyMiddlewares(store, middlewares) {
  middlewares = middlewares.slice();
  middlewares.reverse();
  let dispatch = store.dispatch;
  middlewares.forEach(middleware => {
    dispatch = middleware(store)(dispatch);
  });
  return Object.assign({}, store, {dispatch});

}
