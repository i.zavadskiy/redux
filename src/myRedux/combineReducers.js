/**
 * Created by zavadskyi on 11/27/17.
 */
//combine reducers



var combineReducers = (reducers) => (state, action) => {

  const reducerKeys = Object.keys(reducers);

  let nextState = {};

  for (let i = 0; i < reducerKeys.length; i++) {
    let key = reducerKeys[i];
    let newReducer = reducers[key];
    let stateForKey = state[key];
    nextState[key] = newReducer(action, stateForKey);
  }

  return nextState;

}

export default combineReducers;