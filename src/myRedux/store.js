/**
 * Created by zavadskyi on 11/27/17.
 */

export default function createStore(initState, reducer) {

  if (typeof reducer !== 'function') {
    throw new Error('Expected the reducer to be a function.')
  }

  let currentState = initState;
  let listeners = [];


  function getState() {
    return currentState
  }

  function subscribe(listener = null) {

    if (typeof listener !== 'function') {
      throw new Error('Expected the listener to be a function.')
    }

    listeners.push(listener);

    return function unsubscribe() {
      const index = listeners.indexOf(listener);
      listeners.splice(index, 1);
    }
  }

  function dispatch(action) {
    if (typeof action.type == 'undefined') {
      throw new Error('Action must have "type" param')
    }

    currentState = reducer(currentState, action);

    for (let i = 0; i < listeners.length; i++) {
      const listener = listeners[i];
      listener()
    }

  }


  return {
    getState,
    dispatch,
    subscribe,

  }
}