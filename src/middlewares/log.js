/**
 * Created by zavadskyi on 11/27/17.
 */

export default function log(store) {
  return function (next) {
    return function (action) {
      console.log('log');
      return next(action);
    };
  };
};