/**
 * Created by zavadskyi on 11/27/17.
 */

export default function ping(store) {
  return function (next) {
    return function (action) {
      console.log('ping');
      return next(action);
    };
  };
};